import tkinter as tk
from platformdirs import user_config_dir
import pathlib
import json
            
class Main:
    def __init__(self):
        pass

    def display(self):
        self.root = tk.Tk()
        self.root.title('Biblio')
        dirname = user_config_dir("biblio")
        dirname = pathlib.Path(dirname)
        if not dirname.exists():
            dirname.mkdir(parents=True)        
        filename = dirname / "config.json"
        tk.Label(self.root,
                 text="L'ancien fichier de configuration contenait ceci:"
                 ).pack()
        e = tk.Text(self.root, height=8)
        e.pack(side="top", fill="both", expand=True)
        if filename.exists():
            text = filename.read_text()
            e.insert("1.0", text)
            f2 = pathlib.Path.home() / "biblio_config.json"
            tk.Label(self.root,
                     text=f"Une copie a été sauvée ici: {f2}"
                     ).pack()
            f2.write_bytes(filename.read_bytes())       
        tk.Button(self.root,
                  text="Réinitialiser le fichier de configuration.",
                  command=self.reset).pack()
        tk.Button(self.root,
                  text="Déactiver tout les plugins.",
                  command=self.reset_plugin).pack()

    def reset(self):
        dirname = user_config_dir("biblio")
        dirname = pathlib.Path(dirname)
        filename = dirname / "config.json"        
        with open(filename,"w") as fi:
            json.dump({}, fi,
                      indent=3)

    def reset_plugin(self):
        dirname = user_config_dir("biblio")
        dirname = pathlib.Path(dirname)
        filename = dirname / "config.json"
        config = json.loads(filename.read_text())
        list_ = config.get("plugin", [])
        list_ = [f"#{ll}" for ll in list_]
        config["plugin"] = list_
        filename.write_text(
            json.dumps(config,
                       indent=3)
        )

if __name__ == "__main__":
    m = Main()
    m.display()
    m.root.mainloop()

