import tkinter as tk
from tkinter import ttk, filedialog
from ttkthemes import ThemedTk
# from tkinter.ttk import ttk, filedialog
from platformdirs import user_config_dir
import pathlib
import json
import pandas as pd
import io
import requests


class Tracker:
    """ Toplevel windows resize event tracker. """

    def __init__(self, main):
        self.main = main
        self.toplevel = main.root
        self.width, self.height = self.toplevel.winfo_width(), self.toplevel.winfo_height()
        self._func_id = None

    def bind_config(self):
        self._func_id = self.toplevel.bind("<Configure>", self.resize)

    def unbind_config(self):  # Untested.
        if self._func_id:
            self.toplevel.unbind("<Configure>", self._func_id)
            self._func_id = None

    def resize(self, event):
        if(event.widget == self.toplevel and
           (self.width != event.width or self.height != event.height)):
            #print(f'{event.widget=}: {event.height=}, {event.width=}\n')
            self.width, self.height = event.width, event.height
            self.main.canvas.config(width=self.width-20)
            self.main.canvas.create_window((0, 0),
                                           window=self.main.scrollable_frame,
                                           width=self.width-20, anchor="nw")
class Tooltip:
    def __init__(self, widget, text):
        self.widget = widget
        self.text = text
        self.tooltip = None
        self.widget.bind("<Enter>", self.show)
        self.widget.bind("<Leave>", self.hide)

    def show(self, event=None):
        x, y, _, _ = self.widget.bbox("insert")
        x += self.widget.winfo_rootx() + 25
        y += self.widget.winfo_rooty() + 25

        self.tooltip = tk.Toplevel(self.widget)
        self.tooltip.wm_overrideredirect(True)
        self.tooltip.wm_geometry(f"+{x}+{y}")

        label = ttk.Label(self.tooltip, text=self.text, background="#ffffe0", relief="solid", borderwidth=1)
        label.pack()

    def hide(self, event=None):
        if self.tooltip:
            self.tooltip.destroy()
            self.tooltip = None

            
"""
- Auteur                         -> Auteurs
- Titre                          -> Titre
- Localisation sur les étagères  -> Emplacement
- Collection                     -> Série
- Date d'édition                 -> Date de publication
- Lieu et éditeur                -> Editeur, Edition
- Genre                          -> Genres
- Contenu                        -> Description, Mots-clés associés
- Date d'acquisition             -> Date d'acquisition
- Prêt                           -> Historique des emprunts + Date d'emprunt + Emprunteur
"""

class Main:
    def __init__(self):
        self.popup_dialog = None
        self.config = {}
        self.current_file = ""
        self.current_view = 1
        self.current_view_index = [None]
        self.current_applied_search = None
        self.current_search = [
            ["et", "", None, None, None],
        ]
        self.df_to_use = None
        self.columns = [
        ]
        self.columns_for_quicksearch = [
        ]
        self.columns_for_dates = [
        ]
        self.columns_for_datesminmax = [
        ]
        self.columns_grouped_for_search = [
        ]
        self.load_config()

    def load_config(self):
        dirname = user_config_dir("biblio")
        dirname = pathlib.Path(dirname)
        filename = dirname / "config.json"
        if not filename.exists():
            self.save_config()
        with open(filename, "r") as fi:
            self.config = json.load(fi)
        self.save_config()
        self.apply_config()

    def apply_config(self):
        self.columns = self.config.get("columns", {}).get("columns", self.columns)
        self.columns_for_dates = self.config.get("columns", {}).get("columns_for_dates", self.columns_for_dates)
        self.columns_for_datesminmax = self.config.get("columns", {}).get("columns_for_datesminmax", self.columns_for_datesminmax)
        self.columns_for_quicksearch = self.config.get("columns", {}).get("columns_for_quicksearch", self.columns_for_quicksearch)
        self.columns_grouped_for_search = self.config.get("columns", {}).get("columns_grouped_for_search", self.columns_grouped_for_search)

    def apply_plugin(self):
        dirname = user_config_dir("biblio")
        dirname = pathlib.Path(dirname)
        list_ = self.config.get("plugin", [])
        for plugin_name in list_:
            if plugin_name.startswith("#"):
                continue
            filename2 = dirname / f"{plugin_name}.py"
            if not filename2.exists():
                self.popup(f"Le plugin \"{plugin_name}\" n'est pas installé.\nPlugin ignoré.")
                continue
            try:
                exec(filename2.read_text())
            except Exception as e:
                self.popup(f"Erreur durant l'execution du plugin \"{plugin_name}\":\n{e}\nLe plugin est interrompu.")
        
    def save_config(self):
        if "auto_open_file" not in self.config:
            self.config["auto_open_file"] = ""
        if "number_of_items_per_page" not in self.config:
            self.config["number_of_items_per_page"] = 10
        if "theme" not in self.config:
            self.config["theme"] = "breeze"
        if "latest_directory" not in self.config:
            self.config["latest_directory"] = ""
        if "default_encoding" not in self.config:
            self.config["default_encoding"] = "utf-8"
        if "font_size" not in self.config:
            self.config["font_size"] = 12
        if "font_family" not in self.config:
            self.config["font_family"] = "Arial"
        if "menu_or_button" not in self.config:
            self.config["menu_or_button"] = "les deux"
        if "sort_list" not in self.config:
            self.config["sort_list"] = {
                "Aucun": {
                    "desc": "Aucun tri, utilise l'ordre des éléments dans le fichier.",
                    "sort": [],
                    "keep": True
                    },
                "Auteur": {
                    "desc": "Auteur, puis Titre, puis Date de publication.",
                    "sort": [("Auteurs", "ASC"), ("Titre", "ASC"), ("_Date de publication_clean", "ASC")],
                    "keep": True
                    },
                "Date d'ajout, anciens d'abord": {
                    "desc": "Date d'entrée, puis Auteur, puis Titre.",
                    "sort": [("_Date d'acquisition_clean", "ASC"), ("Auteurs", "ASC"), ("Titre", "ASC")],
                    "keep": True
                    },
                "Date d'ajout, récents d'abord": {
                    "desc": "Date d'entrée, puis Auteur, puis Titre.",
                    "sort": [("_Date d'acquisition_clean", "DESC"), ("Auteurs", "ASC"), ("Titre", "ASC")],
                    "keep": True
                    },                
                "Date de publication, récents d'abord": {
                    "desc": "Date de publication, puis Auteur, puis Titre.",
                    "sort": [("_Date de publication_clean", "DESC"), ("Auteurs", "ASC"), ("Titre", "ASC")],
                    "keep": True
                    },
            }
        if "sort_current" not in self.config:
            self.config["sort_current"] = "Auteur"
        if "do_backup" not in self.config:
            self.config["do_backup"] = "oui"
        if "colors" not in self.config:
            self.config["colors"] = {
                "_left": "#949494",
                "_default": "#000000",
                "Auteurs": '#5aa348',
                "Titre": '#7b60f2',
                "Série": "#1039b5",
                "Editeur": "#107bb5",
                "Edition": "#10b573",
                "Date de publication": "#b57310",
                "Date d'emprunt": "#b25353",
                "Emprunteur": "#b25353",
                "Date d'acquisition": "#b57310",
                }
        if "columns" not in self.config:
            self.config["columns"] = {
                "columns": [
                    "Auteurs",
                    "Titre",
                    "Editeur",
                    "Edition",
                    "Série",            
                    "Date de publication",
                    "Genres",
                    "Description",
                    "Mots-clés associés",
                    "Date d'acquisition",
                    "Emplacement",            
                    "Historique des emprunts",
                    "Date d'emprunt",
                    "Emprunteur",
                ],
                "columns_for_quicksearch": [
                    "Auteurs",
                    "Titre",
                ],
                "columns_for_dates": [
                    "Date de publication",
                    "Date d'acquisition",
                    "Historique des emprunts",
                    "Date d'emprunt",            
                ],
                "columns_for_datesminmax": [
                    "Historique des emprunts",
                ],
                "columns_grouped_for_search": [
                    "Série+Editeur+Edition",
                    "Description+Mots-clés associés",
                    "Date d'emprunt+Emprunteur+Historique des emprunts",
                ]
                }
        if "display" not in self.config:
            self.config["display"] = [
                ["Auteur:", ["Auteurs"]],
                ["Titre:", ["Titre"]],
                ["Édition:", ["Edition", "Editeur", "Série", "Date de publication"]],
                ["Genre:", ["Genres"]],
                ["Contenu:", ["Description", "Mots-clés associés"]],
                ["Date d'ajout:", ["Date d'acquisition"]],
                ["Emplacement:", ["Emplacement"]],
                ["Emprunts:", ["Date d'emprunt", "Emprunteur", "Historique des emprunts"]],
            ]
        if "plugin" not in self.config:
            self.config["plugin"] = []
        dirname = user_config_dir("biblio")
        dirname = pathlib.Path(dirname)
        if not dirname.exists():
            dirname.mkdir(parents=True)        
        filename = dirname / "config.json"
        with open(filename,"w") as fi:
            json.dump(self.config, fi,
                      indent=3)

    def save_fileas(self):
        filetypes = (
            ('CSV files', '*.csv'),
            ('All files', '*.*')
        )
        initialdir = self.config["latest_directory"]
        initialfile = self.current_file
        filename = filedialog.asksaveasfilename(
            title='Choisissez où enregistrer le fichier',
            initialfile=initialfile,
            initialdir=initialdir,
            filetypes=filetypes)
        if filename:
            self.save_file(filename)

    def save_file(self, filename=None):
        if filename is None:
            filename = pathlib.Path(self.config["latest_directory"])
            filename = filename / self.current_file
        if isinstance(filename, str):
            filename = pathlib.Path(filename)
        if filename.exists() and self.config["do_backup"] in [True, "oui"]:
            filename2 = filename.parent
            tag = pd.to_datetime("now").date().isoformat()
            filename2 = filename2 / f"{filename.stem}_{tag}{filename.suffix}_backup"
            if not filename2.exists():
                filename2.write_bytes(filename.read_bytes())
            # delete the oldest backups except the latest 5
            backupfiles = []
            for f in filename.parent.glob(f"{filename.stem}_*{filename.suffix}_backup"):
                backupfiles.append([f.stem, f])
            backupfiles.sort()
            backupfiles = backupfiles[:-5]
            for _, f in backupfiles:
                f.unlink()
        if filename.suffix == ".csv":
            cols = [c for c in self.df.columns if (not c.startswith("_")) & ("+" not in c)]
            self.df[cols].to_csv(filename, index=False)
            self.config["latest_directory"] = str(filename.parent)
            self.save_button.config(style="TButton")
        else:
            self.popup(f'Le fichier\n"{filename.name}"\na une extension inconnue.\nImpossible de sauver.')

    def popup(self, text):
        if self.popup_dialog is not None:
            self.close_popupdiag()
        self.popup_dialog = tk.Toplevel(self.root)
        self.popup_dialog.minsize(200, 100)
        self.popup_dialog.transient(self.root)
        self.popup_dialog.protocol("WM_DELETE_WINDOW", self.close_popupdiag)
        ttk.Label(self.popup_dialog,
                  text=text,
                  font=self.dfont
                  ).pack(side="top", fill="both", expand=True)
        ttk.Button(self.popup_dialog,
                   text="Fermer",
                   command=self.close_popupdiag).pack(
                       side="top", expand=True
                   )

    def close_popupdiag(self):
        self.popup_dialog.destroy()
        self.popup_dialog = None
        self.root.deiconify()
        
    def display(self):
        if self.config["theme"] != "none":
            self.root = ThemedTk(theme=self.config["theme"])
        else:
            self.root = ThemedTk(theme=None)
        self.root.title('Biblio')
        self.dfont = (self.config["font_family"], self.config["font_size"])
        font_small = (self.dfont[0], int(self.dfont[1]*8/12))
        self.root.option_add("*TCombobox*Listbox*Font", self.dfont)
        s = ttk.Style()
        s.configure('TButton', font=self.dfont)
        s.configure('TLabel', font=self.dfont)
        s.configure('TEntry', font=self.dfont)
        s.configure('TText', font=self.dfont)
        s.configure('TMenu', font=self.dfont)
        s.configure("mo.TButton", font=font_small)
        s.configure("sa.TButton", foreground="red")
        #s.map('sa.TButton', foreground=[('active', 'yellow')])
        screen_width = self.root.winfo_screenwidth()
        screen_height = self.root.winfo_screenheight()
        self.root.geometry(f"{screen_width}x{screen_height}")
        self.apply_plugin()
        if self.config["menu_or_button"] != "bouton":
            self.create_menu()
        p = self.config["auto_open_file"]
        if not p:
            self.welcome()
            return
        p = pathlib.Path(p)
        self.load_file(p)

    def create_menu(self):
        menubar = tk.Menu(self.root)
        self.root.config(menu=menubar)
        file_menu = tk.Menu(menubar, tearoff=False)
        file_menu.add_command(
            label='Charger un fichier', font=self.dfont,
            command=self.choose_a_file
        )
        file_menu.add_command(
            label='Sauver le fichier', font=self.dfont,
            command=self.save_file
        )
        file_menu.add_command(
            label='Sauver une copie du fichier', font=self.dfont,
            command=self.save_fileas
        )
        file_menu.add_command(
            label='Quitter', font=self.dfont,
            command=self.root.destroy
        )
        menubar.add_cascade(
            label="Fichier", font=self.dfont,
            menu=file_menu
        )
        manip_menu = tk.Menu(menubar, tearoff=False)
        manip_menu.add_command(
            label='Ajouter un élément', font=self.dfont,
            command=self.click_add
        )
        manip_menu.add_command(
            label='Rechercher', font=self.dfont,
            command=self.click_search
        )
        manip_menu.add_command(
            label='Trier', font=self.dfont,
            command=self.click_sort
        )
        menubar.add_cascade(
            label="Éléments", font=self.dfont,
            menu=manip_menu
        )
        option_menu = tk.Menu(menubar, tearoff=False)
        option_menu.add_command(
            label='Changer les options', font=self.dfont,
            command=self.click_opti
        )
        option_menu.add_command(
            label='Gérer les backups', font=self.dfont,
            command=self.click_backup
        )
        option_menu.add_command(
            label='Exécuter du code', font=self.dfont,
            command=self.click_exec
        )
        option_menu.add_command(
            label='Gérer les plugins', font=self.dfont,
            command=self.click_plug
        )
        option_menu.add_command(
            label='Éditer le fichier de configuration', font=self.dfont,
            command=self.click_cedit
        )        
        menubar.add_cascade(
            label="Options", font=self.dfont,
            menu=option_menu
        )
        
    def clear_display(self):
        list_ = self.root.grid_slaves()
        for l in list_:
            l.destroy()
        list_ = self.root.pack_slaves()
        for l in list_:
            l.destroy()
        list_ = self.root.slaves()
        for l in list_:
            l.destroy()
            
    def welcome(self, message=None):
        frame = ttk.Frame(self.root)
        frame.pack(expand=True)
        if message is None:
            message="Bienvenue sur 'Biblio'."
        ttk.Label(frame,
                  text=message,
                  font=self.dfont
                  ).pack()
        ttk.Label(frame,
                  text="Cliquez sur le bouton ci-dessous pour choisir un fichier.",
                  font=self.dfont
                  ).pack()
        ttk.Label(frame,
                  text="La prochaine fois que vous ouvrirez ce programme, ce fichier sera automatiquement chargé.",
                  font=self.dfont
                  ).pack()
        ttk.Button(frame,
                   text="Choisir un fichier",
                   command=self.choose_a_file
                   ).pack()
        ttk.Label(frame,
                  text=""
                  ).pack(expand=True)

    def choose_a_file(self):
        filetypes = (
            ('CSV files', '*.csv'),
            ('All files', '*.*')
        )
        initialdir = self.config["latest_directory"]
        if not initialdir:
            initialdir = "~"
        filename = filedialog.askopenfilename(
            title='Choisissez un fichier',
            initialdir=initialdir,
            filetypes=filetypes)
        if filename:
            self.clear_display()
            self.load_file(filename)

    def load_file(self, p):
        if isinstance(p, list):
            p = pathlib.Path(p[0])
        if isinstance(p, str):
            p = pathlib.Path(p)
        if isinstance(p, pathlib.Path):
            if not p.exists():
                self.file_not_found(p)
                return
            self.current_file = str(p.resolve())
            self.config["latest_directory"] = str(p.parent.resolve())
            text = ""
            self.encoding = self.config["default_encoding"]
            try:
                text = p.read_text(encoding=self.encoding)
            except UnicodeDecodeError:
                for encoding in ["utf-8", "cp1252", "ascii", "latin_1", "iso8859_2", "cp858", "cp1140"]:
                    try:
                        text = p.read_text(encoding=encoding)
                        self.encoding = encoding
                        break
                    except UnicodeDecodeError:
                        pass
            if not text:
                pass
                #print("Unable to open")
        else:
            self.current_file = p.name
            text = p.content.read().decode(self.config["default_encoding"])
        if text:
            self.load_data(text)
        else:
            self.welcome(f"Erreur lors du chargement du fichier\n{self.current_file}\nChargement annulé.")

    def file_not_found(self, p):
        self.welcome(f"Le fichier {p} est introuvable.")

    def load_data(self, text):
        ttk.Label(self.root,
                  text=f"Chargement du fichier {self.current_file}.",
                  font=self.dfont
                  ).pack()
        if text.splitlines()[0].startswith("<?xml"):
            self.welcome(f"Erreur lors du chargement du fichier\n{self.current_file}\nChargement annulé.")
            return
        sep = ","
        if ";" in text.splitlines()[0]:
            sep = ";"
        file_ = io.StringIO(text)
        try:
            self.df = pd.read_csv(file_, sep=sep, encoding=self.encoding)
        except:
            self.welcome(f"Erreur lors du chargement du fichier\n{self.current_file}\nChargement annulé.")
            return
        try:
            self.df = self.df[self.columns]
        except:
            self.welcome(f"Erreur lors du chargement du fichier\n{self.current_file}\nChargement annulé.")
            return            
        self.df = self.add_usefull_col(self.df)
        self.config["auto_open_file"] = self.current_file
        self.save_config()
        self.display_content()

    def add_usefull_col(self, df):
        df["_filter_txt"] = ""
        for col in self.columns_for_quicksearch:
            df["_filter_txt"] += (df[col].fillna("").astype(str).str.lower()+" ")
        for col in self.columns_for_dates:
            key = "_"+col
            df[key+"_clean"] = [self.clean_date(d) for d in df[col]]
        for col in self.columns_for_datesminmax:
            key = "_"+col
            df[key+"_clean_max"] = [self.clean_date(d, "max") for d in df[col]]
            df[key+"_clean_min"] = [self.clean_date(d, "min") for d in df[col]]
        for col in self.columns_grouped_for_search:
            df[col] = ""
            for col2 in col.split("+"):
                df[col] += (df[col2].fillna("").astype(str).str.lower()+" ")
        return df

    def clean_date(self, text, mode="first"):
        if text is None:
            return ""
        if text != text:
            return ""
        if not isinstance(text, str):
            text = f"{text}"
        number_found = []
        text2 = text.replace("(", " ")
        authorized = "abcdefghijklmnopqrstuvwxyz"
        authorized += authorized.upper()
        authorized += "0123456789"
        authorized += "-/"
        for word in text2.split():
            nword = ''.join([l for l in word if l in authorized])
            if nword.isdigit():
                number_found.append(nword)
            elif len(nword) == 7 and nword[2] == "-" and nword[:2].isdigit() and nword[3:].isdigit():
                number_found.append(nword[3:]+"-"+nword[:2])
            elif len(nword) == 7 and nword[2] == "/" and nword[:2].isdigit() and nword[3:].isdigit():
                number_found.append(nword[3:]+"-"+nword[:2])
            elif len(nword) == 6 and nword[1] == "-" and nword[:1].isdigit() and nword[2:].isdigit():
                number_found.append(nword[2:]+"-0"+nword[:1])
            elif len(nword) == 5 and nword[2] == "-" and nword[:2].isdigit() and nword[3:].isdigit():
                if int(nword[3:]) > 60:
                    number_found.append("19"+nword[3:]+"-"+nword[:2])
                else:
                    number_found.append("20"+nword[3:]+"-"+nword[:2])
            elif len(nword) == 10 and nword[2] == "-" and nword[5] == "-" and nword[:2].isdigit() and nword[3:5].isdigit() and nword[6:].isdigit():
                number_found.append(nword[6:]+"-"+nword[3:5]+"-"+nword[:2])
            elif len(nword) == 10 and nword[2] == "/" and nword[5] == "/" and nword[:2].isdigit() and nword[3:5].isdigit() and nword[6:].isdigit():
                number_found.append(nword[6:]+"-"+nword[3:5]+"-"+nword[:2])
            elif ("".join([l for l in nword if l in authorized[:-2]])).isdigit():
                number_found.append("".join([l for l in nword if l in authorized[:-2]]))
        if number_found:
            if mode == "first":
                return number_found[0]
            number_found.sort()
            if mode == "max":
                return number_found[-1]
            if mode == "min":
                return number_found[0]
        return " "+text.lower()
    
    def display_content(self):
        self.clear_display()
        frame0 = ttk.Frame(self.root)
        frame0.pack(side="top", anchor="w", expand=True, fill="x")
        self.top_infoframe = ttk.Frame(frame0)
        self.top_infoframe.pack(side="left", anchor="w")
        self.top_info = ttk.Label(self.top_infoframe,
                                  font=self.dfont,
                                  text="")
        self.top_info.grid(column=0, columnspan=3, row=0, sticky="EW")
        self.top_info2 = ttk.Label(self.top_infoframe,
                                   font=self.dfont,
                                   text="")
        self.top_info2.grid(column=0, row=1, sticky="EW")
        if self.config["menu_or_button"] != "menu":
            ttk.Button(frame0, text="Configurer",
                       command=self.click_optipre
                       ).pack(side="right", anchor="w", padx=2)
            ttk.Button(frame0, text="Trier",
                       command=self.click_sort
                       ).pack(side="right", anchor="w", padx=2)
            ttk.Button(frame0, text="Rechercher",
                       command=self.click_search
                       ).pack(side="right", anchor="w", padx=2)
            ttk.Button(frame0, text="Charger",
                       command=self.choose_a_file
                       ).pack(side="right", anchor="w", padx=2)
        self.save_button = ttk.Button(frame0, text="Sauver",
                                      command=self.save_file
                                      )
        self.save_button.pack(side="right", anchor="w", padx=2)
        self.save_button.config(style="TButton")
        if self.config["menu_or_button"] != "menu":
            ttk.Button(frame0, text="Ajouter",
                       command=self.click_add
                       ).pack(side="right", anchor="w", padx=2)
        frame1 = ttk.Frame(self.root)
        frame1.pack(anchor="w", fill="x")
        quick_filter_label = ttk.Label(frame1,
                                       font=self.dfont,
                                       text="Filtre rapide:")
        quick_filter_label.pack(side="left")
        self.quick_filter_value = ""
        self.quick_filter = ttk.Entry(frame1, font=self.dfont, text="")
        self.quick_filter.pack(side="left")
        self.quick_filter.bind("<KeyRelease>", self.change_quick_filter)
        frame_bottom = ttk.Frame(self.root)
        frame_bottom.pack(side="top", fill="x", expand=True)
        ttk.Label(frame_bottom, text="").pack(side="left", anchor="w", expand=True, fill="x")
        ttk.Button(frame_bottom, text="Début",
                   command=lambda: self.click_go("home")
                   ).pack(side="left", anchor="w", padx=2)
        ttk.Button(frame_bottom, text="Page précédente",
                   command=lambda: self.click_go("p-1")
                   ).pack(side="left", anchor="w", padx=2)
        ttk.Button(frame_bottom, text="Élément précédent",
                   command=lambda: self.click_go("e-1")
                   ).pack(side="left", anchor="w", padx=2)
        n = int(len(self.df) / self.config["number_of_items_per_page"])+1
        ttk.Label(frame_bottom, text=f"  page", font=self.dfont).pack(side="left", anchor="w")
        self.page_combo = ttk.Combobox(frame_bottom, font=self.dfont, width=4)
        self.page_combo.pack(side="left", anchor="w")
        self.page_combo_end = ttk.Label(frame_bottom, text=f"/{n}  ", font=self.dfont)
        self.page_combo_end.pack(side="left", anchor="w")
        self.page_combo.values = list(range(1, n+1))
        self.page_combo['values'] = self.page_combo.values
        self.page_combo.set(1)
        self.page_combo.bind('<<ComboboxSelected>>', self.set_page_combo)
        ttk.Button(frame_bottom, text="Élément suivant",
                   command=lambda: self.click_go("e+1")
                   ).pack(side="left", anchor="w", padx=2)
        ttk.Button(frame_bottom, text="Page suivante",
                   command=lambda: self.click_go("p+1")
                   ).pack(side="left", anchor="w", padx=2)
        ttk.Button(frame_bottom, text="Fin",
                   command=lambda: self.click_go("end")
                   ).pack(side="left", anchor="w", padx=2)
        ttk.Label(frame_bottom, text="").pack(side="left", anchor="w", expand=True, fill="x")
        container = ttk.Frame(self.root)
        screen_width = self.root.winfo_screenwidth()
        self.canvas = tk.Canvas(container)
        scrollbar = ttk.Scrollbar(container, orient="vertical", command=self.canvas.yview)
        self.scrollable_frame = ttk.Frame(self.canvas)
        self.scrollable_frame.bind(
            "<Configure>",
            lambda e: self.canvas.configure(
                scrollregion=self.canvas.bbox("all"),
                height=e.height
            )
        )
        self.root.bind_all("<MouseWheel>", self._on_mousewheel)
        self.root.bind_all("<Button-4>", self._on_mousewheel)
        self.root.bind_all("<Button-5>", self._on_mousewheel)
        self.canvas.config(width=screen_width-100)
        self.canvas.create_window((0, 0), window=self.scrollable_frame, width=screen_width-100, anchor="nw")
        self.canvas.configure(yscrollcommand=scrollbar.set)
        tracker = Tracker(self)
        tracker.bind_config()
        self.modified_sorting = True
        self.modified_quickfiltering = True
        self.modified_filtering = True
        self.modified_position = True
        timer = self.root.after(2, self.update_itemframe)
        container.pack(side="left", fill="both")
        self.canvas.pack(side="left", fill="both", expand=True)
        scrollbar.pack(side="right", fill="y")

    def set_page_combo(self, e):
        page = self.page_combo.current()
        page = self.page_combo.values[page]
        n = self.config["number_of_items_per_page"]
        self.current_view = ((page-1)*n)+1
        #print("move to element", page, "item", self.current_view)
        self.change_current_view()
        
    def click_go(self, where):
        n = self.config["number_of_items_per_page"]
        v_max = len(self.df_current)-n+1
        if where == "home":
            self.current_view = 1
        if where == "end":
            self.current_view = v_max
        d = 0
        if where == "p-1":
            d = -1*n
        if where == "e-1":
            d = -1
        if where == "p+1":
            d = n
        if where == "e+1":
            d = 1
        v = self.current_view + d
        v = max(1, v)
        v = min(v_max, v)
        self.current_view = v
        page = int(v/n)+1
        self.page_combo.set(page)
        #print("go to element", page, "item", self.current_view)
        self.change_current_view()

    def change_current_view(self):
        v = self.current_view
        index_new = self.df_current[v-1:].index[0]
        if index_new != self.current_view_index[0]:
            n = self.config["number_of_items_per_page"]
            n = max(n, 2)
            n = min(n, 10)
            self.current_view_index = self.df_current[v-1:].index.tolist()[:n]
            self.modified_position = True
            self.update_itemframe()

    def _on_mousewheel(self, event):
        count = 0
        if event.num == 5 or event.delta <= -120:
            count -= 1
        if event.num == 4 or event.delta >= 120:
            count += 1
        self.canvas.yview_scroll(count, "units")
        
    def change_quick_filter(self, e):
        new_value = e.widget.get()
        if new_value != self.quick_filter_value:
            self.quick_filter_value = new_value
            self.modified_quickfiltering = True
            self.update_itemframe()

    def update_itemframe(self):
        redo_top_info = False
        redo_itemframe = False
        if self.modified_quickfiltering or self.modified_filtering:
            if self.df_to_use is None:
                self.df_current = self.df.copy()
            else:
                self.df_current = self.df_to_use.copy()
            value = self.quick_filter_value
            value = value.lower()
            if value:
                if value.startswith('"') and value.endswith('"'):
                    value = [value[1:-1]]
                else:
                    value = value.split()
                for v in value:
                    self.df_current = self.df_current[self.df_current["_filter_txt"].str.contains(v)]
            self.df_current = self.apply_filtering(self.df_current)
            redo_top_info = True
            redo_itemframe = True
        if self.modified_sorting:
            self.df_current = self.apply_sorting(self.df_current)
            redo_itemframe = True
            redo_top_info = True
        if self.modified_position:
            redo_itemframe = True
        if redo_top_info:
            n0 = len(self.df)
            n1 = len(self.df_current)
            if n0 == n1:
                self.top_info.config(text=f"Fichier: {self.current_file}\n{n0} éléments au total.")
            else:
                self.top_info.config(text=f"Fichier: {self.current_file}\n{n1} éléments sélectionnés, sur {n0} au total.")
            list_ = self.top_infoframe.grid_slaves()
            button = None
            for l in list_:
                if "Button" in f"{type(l)}":
                    button = l
            if self.current_applied_search:
                more = f"Tri \"{self.config['sort_current']}\" appliqué. Recherche appliquée."
                if button is None:
                    ttk.Button(self.top_infoframe, text="Désactiver la recherche",
                               command=self.click_unsearch,
                               ).grid(column=1, row=1, padx=3)
            else:
                more = f'Tri "{self.config["sort_current"]}" appliqué.'
                if button:
                    button.destroy()
            self.top_info2.config(text=more)
        if redo_itemframe:
            list_ = self.scrollable_frame.grid_slaves()
            for l in list_:
                l.destroy()
            list_ = self.scrollable_frame.pack_slaves()
            for l in list_:
                l.destroy()
            list_ = self.scrollable_frame.slaves()
            for l in list_:
                l.destroy()
            self.fill_up_itemframe()
        self.modified_sorting = False
        self.modified_quickfiltering = False
        self.modified_filtering = False
        self.modified_position = False

    def click_unsearch(self):
        self.current_applied_search = None
        timer = self.root.after(100, self.unsearch2)

    def unsearch2(self):
        self.modified_filtering = True
        self.update_itemframe()
        
    def apply_filtering(self, df):
        if self.current_applied_search:
            cdf = df.copy()
            current_mask = self.df.index == self.df.index
            for search in self.current_applied_search:
                if search[1] is None:
                    continue
                if search[1] == "":
                    continue
                if search[2] is None:
                    continue
                if search[2] == "":
                    continue
                col = search[1]
                action = search[2]
                new_mask = current_mask.copy()
                value = search[3]
                if value is None:
                    value = ""
                value = value.lower()
                if action == "contient":
                    new_mask = cdf[col].astype(str).str.lower().str.contains(value)
                elif action == "contient":
                    new_mask = cdf[col].astype(str).str.lower().str.startswith(value)
                elif action == "ne contient pas":
                    new_mask = ~cdf[col].astype(str).str.lower().str.contains(value)
                elif action == "est vide":
                    new_mask = cdf[col].isnull() | (cdf[col] == "")
                elif action == "n'est pas vide":
                    new_mask = ~cdf[col].isnull() & (cdf[col] != "")
                elif action == "est supérieur à":
                    col2 = ""
                    if "_"+col+"_clean" in cdf.columns:
                        col2 = "_"+col+"_clean"
                    if "_"+col+"_clean_max" in cdf.columns:
                        col2 = "_"+col+"_clean_max"
                    if col2:
                        value = self.clean_date(value)
                        new_mask = (cdf[col2].astype(str).str.lower() > value)
                    else:
                        new_mask = (cdf[col].astype(str).str.lower() > value)
                elif action == "est inférieur à":
                    col2 = ""
                    if "_"+col+"_clean" in cdf.columns:
                        col2 = "_"+col+"_clean"
                    if "_"+col+"_clean_min" in cdf.columns:
                        col2 = "_"+col+"_clean_min"
                    if col2:
                        value = self.clean_date(value)
                        new_mask = (cdf[col2].astype(str).str.lower() < value)
                    else:
                        new_mask = (cdf[col].astype(str).str.lower() < value)
                mode = search[0]
                if mode == "et":
                    current_mask = current_mask & new_mask
                elif mode == "ou":
                    current_mask = current_mask | new_mask
            df = df[current_mask]
        return df

    def apply_sorting(self, df):
        name = self.config["sort_current"]
        sorting = self.config["sort_list"].get(name, {}).get("sort", [])
        sorting = sorting[::-1]
        for col, d in sorting:
            if col == "index":
                df = df.sort_index(ascending=(d=="ASC"))
                continue
            if col in df.columns:
                df = df.sort_values(col, ascending=(d=="ASC"))
            else:
                self.popup(f"Échec du tri à cause de la colonne {col} ({df.columns})")
        return df

    def fill_up_itemframe(self):
        n = self.config["number_of_items_per_page"]
        current_view_index = None
        if len(self.current_view_index) == 0 or self.current_view_index[0] is None:
            pass
        else:
            for i in self.current_view_index:
                if i in self.df_current.index:
                    current_view_index = i
                    break
        if current_view_index is None:
            cdf = self.df_current[:n]
            self.page_combo.set(1)
            np = int(len(self.df_current)/n)+1
            self.page_combo_end.config(text=f"/{np}  ")
            self.page_combo.values = list(range(1, np+1))
            self.page_combo["values"] = self.page_combo.values
        else:
            i = self.df_current.index.tolist().index(current_view_index)
            cdf = self.df_current[i:i+n]
            self.page_combo.set(int(i/n)+1)
            np = int(len(self.df_current)/n)+1
            self.page_combo_end.config(text=f"/{np}  ")
            self.page_combo.values = list(range(1, np+1))
            self.page_combo["values"] = self.page_combo.values
        # screen_width = self.root.winfo_screenwidth()
        font_small = (self.dfont[0], int(self.dfont[1]*8/12))
        s = ttk.Style()
        #s.configure('.', font=font_small)
        s.configure("mo.TButton", font=font_small)
        for index, row in cdf.iterrows():
            self.draw_element(index, row)

    def draw_element(self, index, row):
        small_frame = ttk.Frame(self.scrollable_frame)
        small_frame.pack(anchor="e", side="top", expand=True, pady=2)
        ttk.Button(small_frame, text="Modifier",
                   style="mo.TButton",
                   command=self.click_modify_callback(index)
                   ).pack(side="right", anchor="e", fill="x", expand=True)
        for line in self.config["display"]:
            self.draw_line(row, line[0], line[1])
        # self.draw_line(row, "Auteur:", ["Auteurs"])
        # self.draw_line(row, "Titre:", ["Titre"])
        # self.draw_line(row, "Édition:", ["Edition", "Editeur", "Série", "Date de publication"])
        # self.draw_line(row, "Genre:", ["Genres"])
        # self.draw_line(row, "Contenu:", ["Description", "Mots-clés associés"])
        # self.draw_line(row, "Date d'ajout:", ["Date d'acquisition"])
        # self.draw_line(row, "Emplacement:", ["Emplacement"])
        # self.draw_line(row, "Emprunts:", ["Date d'emprunt", "Emprunteur", "Historique des emprunts"])
        separator = ttk.Separator(self.scrollable_frame,
                                  orient='horizontal')
        separator.pack(fill='x')

    def draw_line(self, row, left_text, columns):
        tab = 12
        font_norm = self.dfont
        font_small = (self.dfont[0], int(self.dfont[1]*8/12))
        colors = self.config["colors"]
        cl_left = colors["_left"]
        cl_default = colors["_default"]
        small_frame = ttk.Frame(self.scrollable_frame)
        small_frame.pack(anchor="w", side="top", expand=True, pady=2)
        la = ttk.Label(small_frame,
                  text=left_text, width=tab, foreground=cl_left,
                  font=font_norm
                  )
        la.pack(side="left", anchor="w")
        tooltip = Tooltip(la, ", ".join(columns))
        first = True
        for col in columns:
            text = row[col]
            if self.strip(text):
                la2 = ttk.Label(small_frame,
                                text=text,
                                foreground=colors.get(col, cl_default),
                                font=font_norm
                                )
                if first:
                    la2.pack(side="left", anchor="w", padx=1)
                    first = False
                else:
                    la2.pack(side="left", anchor="w", padx=3)
                tooltip = Tooltip(la2, col)
                

    def click_modify_callback(self, i):
        def _callback():
            return self.click_modify(i)
        return _callback

    def get_df_value(self, index, col):
        v = self.df.loc[index, col]
        if v is None or v != v:
            v = ""
        return v
    
    def click_modify(self, i):
        #print("clicked on", i)
        self.modidiag_dialog = tk.Toplevel(self.root)
        self.modidiag_dialog.minsize(600, 100)
        # Tell the window manager, this is the child widget.
        # Interesting, if you want to let the child window 
        # flash if user clicks onto parent
        self.modidiag_dialog.transient(self.root)
        # This is watching the window manager close button
        # and uses the same callback function as the other buttons
        # (you can use which ever you want, BUT REMEMBER TO ENABLE
        # THE PARENT WINDOW AGAIN)
        self.modidiag_dialog.protocol("WM_DELETE_WINDOW", self.close_modidiag)
        y = 0
        cols = [c for c in self.df.columns if (not(c.startswith("_"))) & ("+" not in c)]
        self.modidiag_v = {"i": i}
        for col in cols:
            ttk.Label(self.modidiag_dialog,
                      text=col,
                      font=self.dfont
                      ).grid(column=0, row=y, sticky="EW")
            self.modidiag_v[col] =tk.StringVar()
            self.modidiag_v[col].set(self.get_df_value(i, col))
            ttk.Entry(
                self.modidiag_dialog, textvariable = self.modidiag_v[col]
            ).grid(column=1, columnspan=4, row=y, sticky="EW")
            y += 1
        ttk.Button(self.modidiag_dialog,
                   text='Effacer cet élément',
                   command=self.erase_modidiag
                   ).grid(column=0, columnspan=5, pady=3, row=y+1, sticky="EW")
        frame = ttk.Frame(self.modidiag_dialog)
        frame.grid(column=0, columnspan=5, row=y+2, pady=3)
        ttk.Button(frame,
                   text='Appliquer',
                   command=self.apply_modidiag
                   ).pack(side='left', fill='x', padx=3, pady=1, expand=True)
        ttk.Button(frame,
                   text='Appliquer et fermer',
                   command=self.applyclose_modidiag
                   ).pack(side='left', fill='x', padx=3, pady=1, expand=True)
        ttk.Button(frame,
                   text='Abandonner les modifications',
                   command=self.close_modidiag
                   ).pack(side='right', fill='x', padx=3, pady=1, expand=True)

    def erase_modidiag(self):
        self.modidiag_dialog2 = tk.Toplevel(self.root)
        self.modidiag_dialog2.minsize(100, 50)
        self.modidiag_dialog2.transient(self.root)
        self.modidiag_dialog2.protocol("WM_DELETE_WINDOW", self.close_modidiag2)
        ttk.Label(self.modidiag_dialog2,
                  text='Confirmer que vous voulez effacer cet élément',
                  font=self.dfont
                  ).grid(column=0, columnspan=2, row=0, padx=3, pady=1, sticky="EW")
        ttk.Button(self.modidiag_dialog2,
                   text='Oui, effacer',
                   command=self.erase_modidiag2
                   ).grid(column=0, row=1, padx=3, pady=1, sticky="EW")
        ttk.Button(self.modidiag_dialog2,
                   text='Non',
                   command=self.close_modidiag2
                   ).grid(column=1, row=1, padx=3, pady=1, sticky="EW")

    def erase_modidiag2(self):
        i = self.modidiag_v["i"]
        self.df = self.df[self.df.index != i]
        self.save_button.config(style="sa.TButton")
        self.modified_sorting = True
        self.modified_quickfiltering = True
        self.modified_filtering = True
        self.modified_position = True
        self.update_itemframe()
        self.close_modidiag2()
        self.close_modidiag()

    def close_modidiag2(self):
        self.modidiag_dialog2.destroy()
        
    def apply_modidiag(self):
        i = self.modidiag_v["i"]
        modified = False
        for col in self.modidiag_v:
            if col == "i":
                continue
            v1 = self.modidiag_v[col].get()
            v2 = self.get_df_value(i, col)
            if v1 != v2:
                modified = True
                #print("Modify", col, v2, "->", v1)
                self.df.loc[i, col] = v1
        if modified:
            cdf = self.df[self.df.index == i]
            cdf = self.add_usefull_col(cdf)
            for col in self.df.columns:
                self.df.loc[i, col] = cdf.loc[i, col]
            self.save_button.config(style="sa.TButton")
            self.modified_sorting = True
            self.modified_quickfiltering = True
            self.modified_filtering = True
            self.modified_position = True
            self.update_itemframe()

    def applyclose_modidiag(self):
        self.apply_modidiag()
        self.close_modidiag()
        
    def close_modidiag(self):
        #self.root.wm_attributes("-disabled", False) # IMPORTANT!
        self.modidiag_dialog.destroy()
        # Possibly not needed, used to focus parent window again
        self.root.deiconify() 

    def click_add(self):
        self.adddiag_dialog = tk.Toplevel(self.root)
        self.adddiag_dialog.minsize(600, 100)
        # Tell the window manager, this is the child widget.
        # Interesting, if you want to let the child window 
        # flash if user clicks onto parent
        self.adddiag_dialog.transient(self.root)
        # This is watching the window manager close button
        # and uses the same callback function as the other buttons
        # (you can use which ever you want, BUT REMEMBER TO ENABLE
        # THE PARENT WINDOW AGAIN)
        self.adddiag_dialog.protocol("WM_DELETE_WINDOW", self.close_adddiag)
        y = 0
        cols = [c for c in self.df.columns if (not(c.startswith("_"))) & ("+" not in c)]
        self.adddiag_v = {}
        for col in cols:
            ttk.Label(self.adddiag_dialog,
                      text=col, font=self.dfont
                      ).grid(column=0, row=y, sticky="EW")
            self.adddiag_v[col] =tk.StringVar()
            ttk.Entry(
                self.adddiag_dialog, textvariable = self.adddiag_v[col]
            ).grid(column=1, columnspan=4, row=y, sticky="EW")
            y += 1
        frame = ttk.Frame(self.adddiag_dialog)
        frame.grid(column=0, columnspan=5, row=y, pady=3)
        ttk.Button(frame,
                   text='Créer ce nouvel élément et fermer cette fenêtre',
                   command=self.applyclose_adddiag
                   ).pack(side='left', padx=3, pady=1, fill='x', expand=True)
        ttk.Button(frame,
                   text='Abandonner les modifications',
                   command=self.close_adddiag
                   ).pack(side='right', padx=3, pady=1, fill='x', expand=True)

    def apply_adddiag(self):
        data = {}
        for col in self.adddiag_v:
            v1 = self.adddiag_v[col].get()
            if v1:
                self.save_button.config(style="sa.TButton")
                data[col] = v1
            else:
                data[col] = ""                
        if data:
            nindex = len(self.df)
            while nindex in self.df.index:
                nindex += 1
            ndf = pd.DataFrame(data, index=[nindex])
            ndf = self.add_usefull_col(ndf)
            self.df = pd.concat([self.df, ndf])
            self.modified_sorting = True
            self.modified_quickfiltering = True
            self.modified_filtering = True
            self.modified_position = True
            self.update_itemframe()

    def applyclose_adddiag(self):
        self.apply_adddiag()
        self.close_adddiag()
        
    def close_adddiag(self):
        #self.root.wm_attributes("-disabled", False) # IMPORTANT!
        self.adddiag_dialog.destroy()
        # Possibly not needed, used to focus parent window again
        self.root.deiconify() 

    def click_sort(self):
        self.sortdiag_dialog = tk.Toplevel(self.root)
        self.sortdiag_dialog.minsize(600, 200)
        self.sortdiag_dialog.transient(self.root)
        self.sortdiag_dialog.protocol("WM_DELETE_WINDOW", self.close_sortdiag)
        ttk.Label(self.sortdiag_dialog, font=self.dfont,
                  text="Choisir le tri que vous voulez appliquer"
                  ).pack(side="top", expand=True, fill="both")
        sort_list = self.config["sort_list"]
        sort_names = list(sort_list.keys())
        sort_names.sort()
        self.sort_obj = {}
        self.sort_obj["pre_cb"] = ttk.Combobox(self.sortdiag_dialog,
                                        font=self.dfont)
        self.sort_obj["pre_cb"].pack(side="top", expand=True, fill="both")
        self.sort_obj["pre_cb"]["values"] = sort_names
        self.sort_obj["pre_cb"]["state"] = "readonly"
        self.sort_obj["pre_cb"].bind('<<ComboboxSelected>>',
                                     self.set_sortpre_combo)
        self.sort_obj["pre_info"] = ttk.Label(self.sortdiag_dialog,
                                       text="", font=self.dfont)
        self.sort_obj["pre_info"].pack(side="top", expand=True, fill="both")
        frame = ttk.Frame(self.sortdiag_dialog)
        frame.pack(side="top", expand=True, fill="both", pady=3)
        ttk.Button(frame,
                   text='Appliquer',
                   command=self.applyclose_sortprediag
                   ).pack(side='left', padx=3, pady=1, fill='x', expand=True)
        ttk.Button(frame,
                   text='Créer un tri personnalisé',
                   command=self.click_sort_cus
                   ).pack(side='right', padx=3, pady=1, fill='x', expand=True)
        ttk.Button(frame,
                   text='Abandonner',
                   command=self.close_sortdiag
                   ).pack(side='right', padx=3, pady=1, fill='x', expand=True)

    def set_sortpre_combo(self, e):
        name = self.sort_obj["pre_cb"].current()
        name = self.sort_obj["pre_cb"]["values"][name]
        sort_list = self.config["sort_list"]
        sort_desc = sort_list.get(name, {}).get("desc", "Sans description")
        self.sort_obj["pre_info"].config(text=sort_desc)
        
    def click_sort_cus(self):
        list_ = self.sortdiag_dialog.pack_slaves()
        for l in list_:
            l.destroy()
        ttk.Label(self.sortdiag_dialog, font=self.dfont,
                  text="Cette fonctionnalité arrive bientôt"
                  ).pack(side="top", expand=True, fill="both")

    def apply_sortprediag(self):
        name = self.sort_obj["pre_cb"].current()
        name = self.sort_obj["pre_cb"]["values"][name]
        self.config["sort_current"] = name
        self.save_config()
        self.df = self.apply_sorting(self.df)
        self.modified_sorting = True
        self.update_itemframe()

    def applyclose_sortprediag(self):
        self.apply_sortprediag()
        self.close_sortdiag()
        
    def close_sortdiag(self):
        self.sortdiag_dialog.destroy()
        self.root.deiconify()

    def click_search(self):
        self.searchdiag_dialog = tk.Toplevel(self.root)
        self.searchdiag_dialog.minsize(600, 400)
        self.searchdiag_dialog.transient(self.root)
        self.searchdiag_dialog.protocol("WM_DELETE_WINDOW", self.close_searchdiag)
        if self.current_applied_search is not None:
            self.current_search = self.current_applied_search[:]
        self.update_search()

    def update_search(self):
        list_ = self.searchdiag_dialog.grid_slaves()
        for l in list_:
            l.destroy()
        self.searchdiag_v = {}
        columns = [c for c in self.df.columns if not(c.startswith("_"))]
        choices = ["",
                   "contient",
                   "commence par",
                   "ne contient pas",
                   "est vide",
                   "n'est pas vide",
                   "est supérieur à",
                   "est inférieur à"]
        grid_max = 0
        for i, search in enumerate(self.current_search):
            text = "L'attribut"
            if i != 0:
                text = "l'attribut"
            if search[0] is not None and search[0] != "":
                self.searchdiag_v[f"l{i}"] = ttk.Label(self.searchdiag_dialog,
                                                       text=text, font=self.dfont
                                                       )
                self.searchdiag_v[f"l{i}"].grid(column=1, row=i, sticky="EW")
                v = search[1]
                self.searchdiag_v[f"c{i}"] = ttk.Combobox(self.searchdiag_dialog,
                                                          font=self.dfont)
                self.searchdiag_v[f"c{i}"].grid(column=2, row=i, sticky="EW")
                grid_max = max(grid_max, 2)
                self.searchdiag_v[f"c{i}"]["state"] = "readonly"
                self.searchdiag_v[f"c{i}"]['values'] = [""]+columns
                self.searchdiag_v[f"c{i}"].set(v)
                self.searchdiag_v[f"c{i}"].bind('<<ComboboxSelected>>', lambda e, i=i: self.search_c_change(i))
            v = search[2]
            if v is not None:
                self.searchdiag_v[f"d{i}"] = ttk.Combobox(self.searchdiag_dialog,
                                                          font=self.dfont)
                self.searchdiag_v[f"d{i}"].grid(column=3, row=i, sticky="EW")
                grid_max = max(grid_max, 3)
                self.searchdiag_v[f"d{i}"]["state"] = "readonly"
                self.searchdiag_v[f"d{i}"]["values"] = choices
                self.searchdiag_v[f"d{i}"].set(v)
                self.searchdiag_v[f"d{i}"].bind('<<ComboboxSelected>>', lambda e, i=i: self.search_d_change(i))
            v = search[3]
            if v is not None:
                self.searchdiag_v[f"e{i}"] = ttk.Entry(self.searchdiag_dialog,
                                                       font=self.dfont, text=v)
                self.searchdiag_v[f"e{i}"].grid(column=4, row=i, sticky="EW")
                v2 = self.searchdiag_v[f"e{i}"].get()
                if v2 != v:
                    self.searchdiag_v[f"e{i}"].insert(0,v)
                grid_max = max(grid_max, 4)
                self.searchdiag_v[f"e{i}"].bind("<KeyRelease>", lambda e, i=i: self.search_e_change(i))
            v = search[4]
            if v is not None:
                self.searchdiag_v[f"a{i+1}"] = ttk.Combobox(self.searchdiag_dialog,
                                                            font=self.dfont, width=3)
                self.searchdiag_v[f"a{i+1}"].grid(column=0, row=i+1, sticky="EW")
                self.searchdiag_v[f"a{i+1}"]["state"] = "readonly"
                self.searchdiag_v[f"a{i+1}"]['values'] = ["", "et", "ou"]
                self.searchdiag_v[f"a{i+1}"].set(v)
                self.searchdiag_v[f"a{i+1}"].bind('<<ComboboxSelected>>', lambda e, i=i: self.search_a_change(i))        
        frame = ttk.Frame(self.searchdiag_dialog)
        frame.grid(column=0, columnspan=grid_max+1, row=i+2, sticky="WE", pady=10)
        ttk.Button(frame,
                   text='Appliquer',
                   command=self.apply_searchdiag
                   ).pack(side='left', padx=3, pady=1, fill='x', expand=True)
        ttk.Button(frame,
                   text='Appliquer et fermer',
                   command=self.applyclose_searchdiag
                   ).pack(side='left', padx=3, pady=1, fill='x', expand=True)
        ttk.Button(frame,
                   text='Abandonner',
                   command=self.close_searchdiag
                   ).pack(side='right', padx=3, pady=1, fill='x', expand=True)

    def search_c_change(self, c):
        combo = self.searchdiag_v[f"c{c}"]
        values = combo["values"]
        index_ = combo.current()
        value = values[index_]
        self.current_search[c][1] = value
        if value == "":
            self.current_search[c][2] = None
            self.current_search[c][3] = None
            self.current_search[c][4] = None
            self.current_search = self.current_search[:c+1]
        else:
            if self.current_search[c][2] is None:
                self.current_search[c][2] = ""
                self.current_search[c][3] = None
                self.current_search[c][4] = None
                self.current_search = self.current_search[:c+1]
        self.update_search()

    def search_d_change(self, c):
        combo = self.searchdiag_v[f"d{c}"]
        values = combo["values"]
        index_ = combo.current()
        value = values[index_]
        self.current_search[c][2] = value
        if value in ["", "est vide", "n'est pas vide"]:
            self.current_search[c][3] = None
            self.current_search[c][4] = ""            
            self.current_search = self.current_search[:c+1]
            self.current_search.append(
                ["", None, None, None, None]
            )
        else:
            self.current_search[c][3] = ""            
        if value == "":
            self.current_search[c][4] = None
            self.current_search = self.current_search[:c+1]
        elif self.current_search[c][4] is None:
            self.current_search[c][4] = ""
            self.current_search = self.current_search[:c+1]
            self.current_search.append(
                ["", None, None, None, None]
            )
        self.update_search()

    def search_e_change(self, c):
        entry = self.searchdiag_v[f"e{c}"]
        value = entry.get()
        self.current_search[c][3] = value

    def search_a_change(self, c):
        combo = self.searchdiag_v[f"a{c+1}"]
        values = combo["values"]
        index_ = combo.current()
        value = values[index_]
        if value == "":
            self.current_search[c][4] = ""
            self.current_search[c+1] = ["", None, None, None, None]
            self.current_search = self.current_search[:c+2]
        else:
            self.current_search[c][4] = value
            self.current_search[c+1] = [value, "", None, None, None]
            self.current_search = self.current_search[:c+2]
        self.update_search()

    def apply_searchdiag(self):
        if self.current_search[0][1] is None:
            return
        if self.current_search[0][1] == "":
            return
        if self.current_search[0][2] is None:
            return
        if self.current_search[0][2] == "":
            return
        self.current_applied_search = self.current_search
        self.modified_sorting = True
        self.modified_quickfiltering = True
        self.modified_filtering = True
        self.modified_position = True
        self.update_itemframe()

    def applyclose_searchdiag(self):
        self.apply_searchdiag()
        self.close_searchdiag()
        
    def close_searchdiag(self):
        self.searchdiag_dialog.destroy()
        self.root.deiconify() 

    def click_optipre(self):
        self.optidiagpre_dialog = tk.Toplevel(self.root)
        self.optidiagpre_dialog.minsize(600, 400)
        self.optidiagpre_dialog.transient(self.root)
        self.optidiagpre_dialog.protocol("WM_DELETE_WINDOW", self.close_optidiagpre)
        ttk.Button(self.optidiagpre_dialog,
                   text='Changer les options',
                   command=lambda: self.apply_optidiagpre("opti")
                   ).pack(side='top', padx=3, pady=1, fill='x', expand=True)
        ttk.Button(self.optidiagpre_dialog,
                   text='Gérer les backups',
                   command=lambda: self.apply_optidiagpre("backup")
                   ).pack(side='top', padx=3, pady=1, fill='x', expand=True)
        ttk.Button(self.optidiagpre_dialog,
                   text='Exécuter du code',
                   command=lambda: self.apply_optidiagpre("exec")
                   ).pack(side='top', padx=3, pady=1, fill='x', expand=True)
        ttk.Button(self.optidiagpre_dialog,
                   text='Gérer les plugins',
                   command=lambda: self.apply_optidiagpre("plug")
                   ).pack(side='top', padx=3, pady=1, fill='x', expand=True)
        ttk.Button(self.optidiagpre_dialog,
                   text='Éditer le fichier de configuration',
                   command=lambda: self.apply_optidiagpre("cedit")
                   ).pack(side='top', padx=3, pady=1, fill='x', expand=True)
        ttk.Button(self.optidiagpre_dialog,
                   text='Fermer',
                   command=self.close_optidiagpre
                   ).pack(side='top', padx=3, pady=1, fill='x', expand=True)

    def apply_optidiagpre(self, what):
        self.close_optidiagpre()
        if what == "opti":
            self.click_opti()
        elif what == "backup":
            self.click_backup()
        elif what == "exec":
            self.click_exec()
        elif what == "plug":
            self.click_plug()
        elif what == "cedit":
            self.click_cedit()        

    def close_optidiagpre(self):
        self.optidiagpre_dialog.destroy()
        self.root.deiconify() 
        
    def click_opti(self):
        self.optidiag_dialog = tk.Toplevel(self.root)
        self.optidiag_dialog.minsize(600, 400)
        self.optidiag_dialog.transient(self.root)
        self.optidiag_dialog.protocol("WM_DELETE_WINDOW", self.close_optidiag)
        dirname = user_config_dir("biblio")
        dirname = pathlib.Path(dirname)
        filename = dirname / "config.json"
        ttk.Label(self.optidiag_dialog, font=self.dfont,
                  text=f"Répertoire du fichier de configuration: {filename}\nCes modifications peuvent nécessiter un redémarrage pour être correctement appliquées."
                  ).pack(side="top", expand=True, fill="both")
        frame0 = ttk.Frame(self.optidiag_dialog)
        frame0.pack(side="top", expand=True, fill="both")
        frame1 = ttk.Frame(self.optidiag_dialog)
        frame1.pack(side="top", expand=True, fill="both")
        frame2 = ttk.Frame(self.optidiag_dialog)
        frame2.pack(side="top", expand=True, fill="both")
        frame3 = ttk.Frame(self.optidiag_dialog)
        frame3.pack(side="top", expand=True, fill="both")
        frame4 = ttk.Frame(self.optidiag_dialog)
        frame4.pack(side="top", expand=True, fill="both")
        frame5 = ttk.Frame(self.optidiag_dialog)
        frame5.pack(side="top", expand=True, fill="both")
        frame6 = ttk.Frame(self.optidiag_dialog)
        frame6.pack(side="top", expand=True, fill="both")

        self.opti_v = {}
        
        ttk.Label(frame0, text="Nombre d'éléments par page",
                  font=self.dfont
                  ).pack(side="left", expand=True, fill="both")
        self.opti_v["ne"] = ttk.Combobox(frame0,
                                         font=self.dfont)
        self.opti_v["ne"].pack(side="left", expand=True, fill="both")
        self.opti_v["ne"]["values"] = ["2", "3", "4", "5", "6", "7", "8", "9", "10", "12", "15", "20", "50", "100"]
        self.opti_v["ne"].set(str(self.config["number_of_items_per_page"]))
        self.opti_v["ne"]["state"] = "readonly"

        ttk.Label(frame1, text="Thème graphique",
                  font=self.dfont
                  ).pack(side="left", expand=True, fill="both")
        self.opti_v["th"] = ttk.Combobox(frame1,
                                         font=self.dfont)
        self.opti_v["th"].pack(side="left", expand=True, fill="both")
        self.opti_v["th"]["values"] = ["none", "arc", "breeze", "clearlooks", "adapta", "keramik", "plastik", "ubuntu", "scidblue", "smog", "winxpblue"]
        self.opti_v["th"].set(self.config["theme"])
        self.opti_v["th"]["state"] = "readonly"

        ttk.Label(frame2, text="Taille de caractère",
                  font=self.dfont
                  ).pack(side="left", expand=True, fill="both")
        self.opti_v["fs"] = ttk.Combobox(frame2,
                                         font=self.dfont)
        self.opti_v["fs"].pack(side="left", expand=True, fill="both")
        self.opti_v["fs"]["values"] = ["6", "7", "8", "9", "10", "11", "12", "13", "14", "15", "16", "17", "18", "19", "20", "21", "22", "23", "24", "25", "26", "27", "28", "29", "30", "36", "42"]
        self.opti_v["fs"].set(str(self.config["font_size"]))
        self.opti_v["fs"]["state"] = "readonly"

        ttk.Label(frame3, text="Nom de la famille de caractère",
                  font=self.dfont
                  ).pack(side="left", expand=True, fill="both")
        self.opti_v["ff"] = ttk.Combobox(frame3,
                                         font=self.dfont)
        self.opti_v["ff"].pack(side="left", expand=True, fill="both")
        self.opti_v["ff"]["values"] = ["Helvetica", "Arial", "Courier", "system"]
        self.opti_v["ff"].set(self.config["font_family"])
        #self.opti_v["ff"]["state"] = "readonly"

        ttk.Label(frame4, text="Encodage par défault (changer avec précaution)",
                  font=self.dfont
                  ).pack(side="left", expand=True, fill="both")
        self.opti_v["en"] = ttk.Combobox(frame4,
                                         font=self.dfont)
        self.opti_v["en"].pack(side="left", expand=True, fill="both")
        self.opti_v["en"]["values"] = ["utf-8", "cp1252", "ascii", "latin_1", "iso8859_2", "cp858", "cp1140"]
        self.opti_v["en"].set(self.config["default_encoding"])
        #self.opti_v["en"]["state"] = "readonly"

        ttk.Label(frame5, text="Afficher le menu ou les boutons",
                  font=self.dfont
                  ).pack(side="left", expand=True, fill="both")
        self.opti_v["mb"] = ttk.Combobox(frame5,
                                         font=self.dfont)
        self.opti_v["mb"].pack(side="left", expand=True, fill="both")
        self.opti_v["mb"]["values"] = ["menu", "bouton", "les deux"]
        self.opti_v["mb"].set(self.config["menu_or_button"])
        self.opti_v["mb"]["state"] = "readonly"

        ttk.Label(frame6, text="Sauvegarder un backup du fichier",
                  font=self.dfont
                  ).pack(side="left", expand=True, fill="both")
        self.opti_v["db"] = ttk.Combobox(frame6,
                                         font=self.dfont)
        self.opti_v["db"].pack(side="left", expand=True, fill="both")
        self.opti_v["db"]["values"] = ["non", "oui"]
        self.opti_v["db"].set(self.config["do_backup"])
        self.opti_v["db"]["state"] = "readonly"

        frame9 = ttk.Frame(self.optidiag_dialog)
        frame9.pack(side="top", fill="both")
        ttk.Button(frame9,
                   text='Exporter la configuration',
                   command=self.export_optidiag
                   ).pack(side='left', padx=3, pady=1, fill='x', expand=True)
        ttk.Button(frame9,
                   text='Importer une configuration',
                   command=self.import_optidiag
                   ).pack(side='left', padx=3, pady=1, fill='x', expand=True)

        
        frame = ttk.Frame(self.optidiag_dialog)
        frame.pack(side="top", fill="both")
        ttk.Button(frame,
                   text='Appliquer',
                   command=self.apply_optidiag
                   ).pack(side='left', padx=3, pady=1, fill='x', expand=True)
        ttk.Button(frame,
                   text='Appliquer et fermer',
                   command=self.applyclose_optidiag
                   ).pack(side='left', padx=3, pady=1, fill='x', expand=True)
        ttk.Button(frame,
                   text='Fermer',
                   command=self.close_optidiag
                   ).pack(side='right', padx=3, pady=1, fill='x', expand=True)

    def import_optidiag(self):
        filetypes = (
            ('JSON files', '*.json'),
            ('All files', '*.*')
        )
        initialdir = "~"
        filename = filedialog.askopenfilename(
            title='Choisissez un fichier de configuration',
            initialdir=initialdir,
            filetypes=filetypes)
        if filename:
            try:
                data = pathlib.Path(filename)
                data = data.read_text()
                data = json.loads(data)
            except:
                return
            self.config = data
            self.apply_config()

    def export_optidiag(self):
        filetypes = (
            ('JSON files', '*.json'),
            ('All files', '*.*')
        )
        initialdir = "~"
        filename = filedialog.asksaveasfilename(
            title='Choisissez où enregistrer le fichier de configuration',
            initialfile="config.json",
            initialdir=initialdir,
            filetypes=filetypes)
        if filename:
            data = pathlib.Path(filename)
            data.write_text(json.dumps(self.config, indent=3))

    def apply_optidiag(self):
        key_to_conf = {
            "ne": "number_of_items_per_page",
            "th": "theme",
            "fs": "font_size",
            "ff": "font_family",
            "en": "default_encoding",
            "mb": "menu_or_button",
            "db": "do_backup"
        }
        for key in self.opti_v:
            combo = self.opti_v[key]
            values = combo["values"]
            index_ = combo.current()
            value = values[index_]
            keyc = key_to_conf.get(key)
            value_old = self.config.get(keyc)
            value_old = f"{value_old}"
            if value_old == value:
                continue
            if key == "ne":
                self.config[keyc] = int(value)
                self.modified_position = True
                self.update_itemframe()
            if key == "th":
                self.config[keyc] = value
                if value != "none":
                    self.root.set_theme(value)
            if key == "fs":
                self.config[keyc] = int(value)
                self.dfont = (self.config["font_family"], self.config["font_size"])
                self.modified_position = True
                self.modified_filtering = True
                self.update_itemframe()
            if key == "ff":
                self.config[keyc] = value
                self.dfont = (self.config["font_family"], self.config["font_size"])
                self.modified_position = True
                self.modified_filtering = True
                self.update_itemframe()
            if key == "en":
                self.config[keyc] = value
            if key == "mb":
                self.config[keyc] = value
            if key == "db":
                self.config[keyc] = value
        self.save_config()

    def applyclose_optidiag(self):
        self.apply_optidiag()
        self.close_optidiag()
        
    def close_optidiag(self):
        self.optidiag_dialog.destroy()
        self.root.deiconify() 
        
    def strip(self, text):
        if text is None:
            return ""
        if text != text:
            return ""
        if not isinstance(text, str):
            text = f"{text}"
        text = text.lower().strip()
        for l in ["'\",- ."]:
            text = text.replace(l, "")
        return text

    def click_exec(self):
        self.execdiag_dialog = tk.Toplevel(self.root)
        self.execdiag_dialog.minsize(600, 400)
        self.execdiag_dialog.transient(self.root)
        self.execdiag_dialog.protocol("WM_DELETE_WINDOW", self.close_execdiag)

        self.exec_obj = tk.Text(
            self.execdiag_dialog  #, textvariable = exec_value
        )
        self.exec_obj.pack(side="top", anchor="w", fill="both", expand=True)
        ttk.Button(self.execdiag_dialog,
                   text='Exécuter le code',
                   command=self.exec_execdiag
                   ).pack(side="top", anchor="w", fill="both", expand=True)

    def close_execdiag(self):
        self.execdiag_dialog.destroy()
        self.root.deiconify()

    def exec_execdiag(self):
        text = self.exec_obj.get('1.0', 'end')
        success = True
        try:
            exec(text)
        except Exception as e:
            success = False
            self.popup(f"Problème lors de l'exécution du code.\n{e}\nAbandon.")
        if success:
            self.popup("Code correctement exécuté.")

    def click_plug(self):
        self.plugdiag_dialog = tk.Toplevel(self.root)
        self.plugdiag_dialog.minsize(600, 400)
        self.plugdiag_dialog.transient(self.root)
        self.plugdiag_dialog.protocol("WM_DELETE_WINDOW", self.close_plugdiag)

        ttk.Button(self.plugdiag_dialog,
                   text='Ajouter un plugin',
                   command=self.new_plugdiag
                   ).pack(side="top", anchor="w", fill="both", expand=True)

        tk.Label(self.plugdiag_dialog,
                 text="Vous pouvez changer l'ordre, ou ajouter le caractère \"#\" pour ignorer un plugin."
        ).pack(side="top", anchor="w", fill="both", expand=True)
        
        self.plug_obj = tk.Text(
            self.plugdiag_dialog
        )
        self.plug_obj.pack(side="top", anchor="w", fill="both", expand=True)
        self.plug_obj.insert('end', "\n".join(self.config["plugin"]))
        frame = ttk.Frame(self.plugdiag_dialog)
        frame.pack(side="top", fill="both")
        ttk.Button(frame,
                   text='Appliquer',
                   command=self.apply_plugdiag
                   ).pack(side='left', padx=3, pady=1, fill='x', expand=True)
        ttk.Button(frame,
                   text='Appliquer et fermer',
                   command=self.applyclose_plugdiag
                   ).pack(side='left', padx=3, pady=1, fill='x', expand=True)
        ttk.Button(frame,
                   text='Fermer',
                   command=self.close_plugdiag
                   ).pack(side='right', padx=3, pady=1, fill='x', expand=True)

    def download_plugin(self, url):
        if url.startswith("."):
            url = url[1:]
            url = f"https://framadrive.org/s/e5qGzpttmqDrXsc/download?path=%2F&files={url}"
        response = requests.get(url)
        if not response.ok:
            self.popup(f"Unable to download {url}")
            return
        data = response.content
        if isinstance(data, str):
            data_str = data
        else:
            data_str = data.decode("utf-8")
        print([f.strip() for f in data_str.splitlines()])
        name = [f.strip() for f in data_str.splitlines() if f.strip().startswith("#name:")]
        if len(name) == 0:
            name = pd.to_datetime("now").isoformat().split(".")[0]+".py"
        else:
            name = name[0].split(":")[1].strip()
            if "." not in name:
                name = name + ".py"
        dirname = user_config_dir("biblio")
        dirname = pathlib.Path(dirname)
        filename2 = dirname / name
        if isinstance(data, str):
            filename2.write_text(data)
        else:
            filename2.write_bytes(data)
        list_ = self.config.get("plugin", [])
        list_.append(name.split(".")[0])
        self.config["plugin"] = list_
        self.save_config()
        
    def apply_plugdiag(self):
        text = self.plug_obj.get('1.0', 'end')
        list_ = [t.strip() for t in text.splitlines() if t.strip()]
        dirname = user_config_dir("biblio")
        dirname = pathlib.Path(dirname)
        for plugin_name in list_:
            if plugin_name.startswith("#"):
                continue
            filename2 = dirname / f"{plugin_name}.py"
            if not filename2.exists():
                self.popup(f"Le plugin \"{plugin_name}\" n'est pas installé.\nVeuillez arranger cela.\nEn attendant, les modifications ne sont pas appliquées.")
                return
        self.config["plugin"] = list_
        self.save_config()

    def applyclose_plugdiag(self):
        self.apply_plugdiag()
        self.close_plugdiag()
        
    def close_plugdiag(self):
        self.plugdiag_dialog.destroy()
        self.root.deiconify()
        
    def new_plugdiag(self):
        filetypes = (
            ('Python files', '*.py'),
            ('All files', '*.*')
        )
        initialdir = self.config["latest_directory"]
        if not initialdir:
            initialdir = "~"
        filename = filedialog.askopenfilename(
            title='Choisissez un fichier',
            initialdir=initialdir,
            filetypes=filetypes)
        if filename:
            if isinstance(filename, str):
                filename = pathlib.Path(filename)
            if not filename.exists():
                self.popup("Le fichier n'existe pas. Abandon.")
                return
            plugin_name = filename.stem
            self.plug_obj.insert('end', f'\n{plugin_name}')
            dirname = user_config_dir("biblio")
            dirname = pathlib.Path(dirname)
            filename2 = dirname / filename.name
            filename2.write_bytes(filename.read_bytes())

    def click_cedit(self):
        self.ceditdiag_dialog = tk.Toplevel(self.root)
        self.ceditdiag_dialog.minsize(600, 400)
        self.ceditdiag_dialog.transient(self.root)
        self.ceditdiag_dialog.protocol("WM_DELETE_WINDOW", self.close_ceditdiag)

        tk.Label(self.ceditdiag_dialog,
                 text="Ces modifications peuvent nécessiter un redémarrage pour être correctement appliquées."
        ).pack(side="top", anchor="w", fill="both", expand=True)
        self.cedit_obj = tk.Text(
            self.ceditdiag_dialog
        )
        self.cedit_obj.pack(side="top", anchor="w", fill="both", expand=True)
        self.cedit_obj.insert('end', json.dumps(self.config, indent=3))
        frame = ttk.Frame(self.ceditdiag_dialog)
        frame.pack(side="top", fill="both")
        ttk.Button(frame,
                   text='Appliquer',
                   command=self.apply_ceditdiag
                   ).pack(side='left', padx=3, pady=1, fill='x', expand=True)
        ttk.Button(frame,
                   text='Appliquer et fermer',
                   command=self.applyclose_ceditdiag
                   ).pack(side='left', padx=3, pady=1, fill='x', expand=True)
        ttk.Button(frame,
                   text='Fermer',
                   command=self.close_ceditdiag
                   ).pack(side='right', padx=3, pady=1, fill='x', expand=True)
        
    def apply_ceditdiag(self):
        text = self.cedit_obj.get('1.0', 'end')
        try:
            data = json.loads(text)
        except Exception as e:
            self.popup(f"Le texte n'est pas un document json correctement formatté.\n{e}\nAbandon.")
            return
        self.config = data
        self.save_config()

    def applyclose_ceditdiag(self):
        self.apply_ceditdiag()
        self.close_ceditdiag()
        
    def close_ceditdiag(self):
        self.ceditdiag_dialog.destroy()
        self.root.deiconify()

    def click_backup(self):
        self.backupdiag_dialog = tk.Toplevel(self.root)
        self.backupdiag_dialog.minsize(600, 400)
        self.backupdiag_dialog.transient(self.root)
        self.backupdiag_dialog.protocol("WM_DELETE_WINDOW", self.close_backupdiag)
        dirname = pathlib.Path(self.config["latest_directory"])
        filename = self.current_file
        suffix = ""
        if isinstance(filename, str):
            filename = pathlib.Path(filename)
        if str(filename.parent) != ".":
            dirname = filename.parent
        suffix = filename.suffix
        filename = filename.stem
        self.backupdial_dfs = {}
        self.backupdial_p = {}
        paths = []
        for p in dirname.glob(f"{filename}_*{suffix}_backup"):
            paths.append([p.name, p])
        paths.sort()
        for _, p in paths:
            name = p.name
            self.backupdial_p[name] = p
            cdf = None
            try:
                try:
                    text = p.read_text(encoding=self.encoding)
                except:
                    text = p.read_text(encoding=self.config["default_encoding"])
                sep = ","
                if ";" in text.splitlines()[0]:
                    sep = ";"
                cdf = pd.read_csv(p, sep=sep, encoding=self.encoding)
            except:
                pass
            self.backupdial_dfs[name] = cdf
            frame = ttk.Frame(self.backupdiag_dialog)
            frame.pack(side="top", fill="both")
            if cdf is None:
                ttk.Label(frame, text=f"{name}: erreur lors du chargement").pack(
                    side='left', fill='x', expand=True
                )
                ttk.Button(frame,
                           text='Effacer',
                           command=lambda name=name: self.delete_backupdiag(name)
                           ).pack(side='left', padx=3, pady=1, fill='x', expand=True)
            else:
                n = len(cdf)
                ttk.Label(frame, text=f"{name}: {n} éléments").pack(
                    side='left', fill='x', expand=True
                )
                ttk.Button(frame,
                           text='Comparer',
                           command=lambda name=name: self.compare_backupdiag(name)
                           ).pack(side='left', padx=3, pady=1, fill='x', expand=True)
                ttk.Button(frame,
                           text='Restaurer',
                           command=lambda name=name: self.restore_backupdiag(name)
                           ).pack(side='left', padx=3, pady=1, fill='x', expand=True)
                ttk.Button(frame,
                           text='Effacer',
                           command=lambda name=name: self.delete_backupdiag(name)
                           ).pack(side='left', padx=3, pady=1, fill='x', expand=True)
            
        frame = ttk.Frame(self.backupdiag_dialog)
        frame.pack(side="top", fill="both")
        #ttk.Button(frame,
        #           text='Appliquer',
        #           command=self.apply_backupdiag
        #           ).pack(side='left', padx=3, pady=1, fill='x', expand=True)
        #ttk.Button(frame,
        #           text='Appliquer et fermer',
        #           command=self.applyclose_backupdiag
        #           ).pack(side='left', padx=3, pady=1, fill='x', expand=True)
        ttk.Button(frame,
                   text='Fermer',
                   command=self.close_backupdiag
                   ).pack(side='right', padx=3, pady=1, fill='x', expand=True)
        
    def delete_backupdiag(self, name):
        p = self.backupdial_p[name]
        p.unlink()
        self.close_backupdiag()
        self.click_backup()
        self.popup(f"Fichier {name} effacé.")

    def restore_backupdiag(self, name):
        p = self.backupdial_p[name]
        initialdir = str(p.parent)
        initialfile = p.stem+".csv"
        filetypes = (
            ('CSV files', '*.csv'),
            ('All files', '*.*')
        )
        filename = filedialog.asksaveasfilename(
            title='Choisissez où enregistrer le fichier',
            initialfile=initialfile,
            initialdir=initialdir,
            filetypes=filetypes)
        if filename:
            if isinstance(filename, str):
                filename = pathlib.Path(filename)
            filename.write_bytes(p.read_bytes())
            self.popup(f"Fichier sauvé ici: {filename}. Veuillez ouvrir ce fichier si vous voulez remplacer le fichier courant.")

    def compare_backupdiag(self, name):
        self.backupdiag_dialog2 = tk.Toplevel(self.root)
        self.backupdiag_dialog2.minsize(600, 400)
        self.backupdiag_dialog2.transient(self.root)
        self.backupdiag_dialog2.protocol("WM_DELETE_WINDOW", self.close_backupdiag2)
        self.backupdiag2_obj = tk.Text(
            self.backupdiag_dialog2
        )
        self.backupdiag2_obj.pack(side="top", anchor="w", fill="both", expand=True)
        self.backupdiag2_obj.insert('end', "Veuillez patienter durant le calcul")
        self.backupdiag2_name = name
        ttk.Button(self.backupdiag_dialog2,
                   text='Fermer',
                   command=self.close_backupdiag2
                   ).pack(side='top', padx=3, pady=1, fill='x', expand=True)
        timer = self.root.after(500, self.compare_backupdiag2)

    def compare_backupdiag2(self):
        name = self.backupdiag2_name
        df1 = self.df.copy()
        df2 = self.backupdial_dfs[name].copy()
        columns = self.columns
        is_in_df1 = []
        is_modified = []
        is_in_df2 = []
        col1 = columns[0]
        col2 = columns[1]
        for index, row in df1.iterrows():
            cdf = df2[(df2[col1] == row[col1]) & (df2[col2] == row[col2])]
            if len(cdf) == 0:
                is_in_df1.append(index)
                continue
            for col in columns:
                index2 = cdf.index[0]
                cdf = cdf[cdf[col].astype(str) == str(row[col])]
                if len(cdf) == 0:
                    is_modified.append([index, index2])
                    break
        for index, row in df2.iterrows():
            cdf = df1[(df1[col1] == row[col1]) & (df1[col2] == row[col2])]
            if len(cdf) == 0:
                is_in_df2.append(index)
        self.backupdiag2_obj.delete('1.0', 'end')
        if not is_in_df1 and not is_in_df2 and not is_modified:
            self.backupdiag2_obj.insert('end', "Pas de différences.")
        if is_in_df1:
            self.backupdiag2_obj.insert('end', "\n---- Les lignes suivantes existent dans le fichier courant mais n'existent pas dans le backup:\n\n")
            for index1 in is_in_df1:
                cdf = df1[df1.index == index1]
                self.backupdiag2_obj.insert('end', f"{col1}: {cdf[col1].iat[0]}, {col2}: {cdf[col2].iat[0]}\n\n")
        if is_in_df2:
            self.backupdiag2_obj.insert('end', "\n---- Les lignes suivantes existent dans le backup mais n'existent pas dans le fichier courant:\n\n")
            for index2 in is_in_df2:
                cdf = df2[df2.index == index1]
                self.backupdiag2_obj.insert('end', f"{col1}: {cdf[col1].iat[0]}, {col2}: {cdf[col2].iat[0]}\n\n")
        if is_modified:
            self.backupdiag2_obj.insert('end', "\n---- Les lignes suivantes ont été modifiées:\n")
            for index1, index2 in is_modified:
                cdf1 = df1[df1.index == index1]
                cdf2 = df2[df2.index == index2]
                col_ex = []
                for col in columns:
                    v1 = cdf1[col].astype(str).iat[0]
                    v2 = cdf2[col].astype(str).iat[0]
                    if v1 != v2:
                        col_ex.append(f" {col}: \"{v2}\" dans le backup, \"{v1}\" dans le fichier courant")
                self.backupdiag2_obj.insert('end', f"\n{col1}: {cdf1[col1].iat[0]}, {col2}: {cdf1[col2].iat[0]}\n")
                for l in col_ex:
                    self.backupdiag2_obj.insert('end', l+"\n")

    def close_backupdiag(self):
        self.backupdial_dfs = {}
        self.backupdiag_dialog.destroy()
        self.root.deiconify()

    def close_backupdiag2(self):
        self.backupdiag_dialog2.destroy()
        self.root.deiconify()

            
if __name__ == "__main__":
    m = Main()
    m.display()
    m.root.mainloop()

